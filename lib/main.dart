import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
//5B8FB9
//51d999
  Color color = const Color(0xff7FB77E);
  Color secondColor = Colors.white;
  TextEditingController textEditingController = TextEditingController();
  Color? backgroundColor = Colors.white;
  int pageIndex = 2;
  List<String> categories = ['الاكثر رواجا','عناية بالبشره','جمال','صحة','اكسسوارات','بيع'];
  int categoeryIndexSelected = 0;
  List<Product> products = [
    Product(name: 'منتج اول', img: 'img/product1.jpg', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 30,000'),
    Product(name: 'منتج ثاني', img: 'img/product2.jpg', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 50,000'),
    Product(name: 'منتج رابع', img: 'img/product2.jpg', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 60,000'),
    Product(name: 'منتج خامس', img: 'img/product1.jpg', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 70,000'),
  ];
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
      backgroundColor: backgroundColor,
      body: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 220,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30)
              ),
              color: color
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('  العنوان',textAlign: TextAlign.right,
                            style: GoogleFonts.almarai(fontSize: 13,color: Colors.white),),
                          const SizedBox(height: 5,),
                          Row(
                            children: [
                              Icon(Icons.location_on_sharp,color: secondColor,size: 20,),
                              const SizedBox(width: 10,),
                              Text('بغداد، العراق',textAlign: TextAlign.right,
                                style: GoogleFonts.almarai(fontSize: 14,color: Colors.white,fontWeight: FontWeight.bold),),
                              const SizedBox(width: 10,),
                              Icon(Icons.keyboard_arrow_down_outlined,color: secondColor,size: 18,),
                            ],
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white24
                            ),
                            child: const Center(
                              child: Icon(Icons.notifications,color: Colors.white,),
                            ),
                          ),
                          const SizedBox(width: 10,),
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white24
                            ),
                            child: const Center(
                              child: Icon(Icons.settings,color: Colors.white,),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  const SizedBox(height: 40,),
                  TextFormField(
                    keyboardType: TextInputType.text,
                    controller: textEditingController,
                    style: GoogleFonts.almarai(fontSize: 14,color: Colors.black87),
                    decoration: InputDecoration(
                      hintText: 'ادخل كلمة البحث',
                      hintStyle: GoogleFonts.almarai(fontSize: 14,color: Colors.black87),
                      prefixIcon: const Icon(Icons.search, color: Colors.black),
                      counterStyle: GoogleFonts.almarai(color: Colors.black87),
                      labelStyle: GoogleFonts.almarai(color: Colors.black87),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(color: Colors.transparent),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(color: Colors.transparent),
                      ),
                      icon: Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white
                        ),
                        child: const Center(
                          child: Icon(Icons.filter_list,color: Colors.black87,),
                        ),
                      ),
                      fillColor: Colors.white,
                      filled: true,
                      isDense: true,
                      contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                      alignLabelWithHint: true,
                    ),
                  ),
                  const SizedBox(height: 20,),
                ],
              )
            ),
          ),
          const SizedBox(height: 20,),
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                CarouselSlider(
                  options: CarouselOptions(
                      height: 200.0,
                      enlargeCenterPage: false,
                      autoPlay: true,
                      pageSnapping:false,
                      aspectRatio: 16/19,
                      enableInfiniteScroll: true,
                      viewportFraction: 0.9,
                    enlargeFactor: 20.0,
                  ),
                  items: ['img/offer2.jpg','img/offer.jpg','img/offer3.jpg'].map((e) {
                    return InkWell(
                      onTap: ()async{},
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset(e,width: MediaQuery.of(context).size.width,fit: BoxFit.fill,),
                              ),
                            ),
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  gradient: const LinearGradient(
                                    colors: [Colors.black87,Colors.transparent],
                                    begin: Alignment.bottomRight,
                                    end: Alignment.topLeft
                                  )
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text('عروض اليوم',textAlign: TextAlign.right,
                                    style: GoogleFonts.almarai(fontSize: 20,color: Colors.white,fontWeight: FontWeight.bold),),
                                    const SizedBox(height: 10,),
                                    Container(
                                      width: 110,
                                      height: 25,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: backgroundColor
                                      ),
                                      child: Center(
                                        child: Text('تصفح',textAlign: TextAlign.right,
                                          style: GoogleFonts.almarai(fontSize: 13,color: Colors.black),),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                ),
                const SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 13),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('ابرز الاقسام',textAlign: TextAlign.right,
                        style: GoogleFonts.almarai(fontSize: 17,color: Colors.black,fontWeight: FontWeight.bold),),
                      Text('رؤية الكل',textAlign: TextAlign.right,
                        style: GoogleFonts.almarai(fontSize: 12,color: Colors.green,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
                const SizedBox(height: 10,),
                SizedBox(
                  height: 40,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                    itemCount: categories.length,
                    scrollDirection: Axis.horizontal,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    itemBuilder: (BuildContext context,int index){
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: GestureDetector(
                          onTap: (){
                            setState(() {
                              categoeryIndexSelected = index;
                            });
                          },
                          child: AnimatedContainer(
                            padding: const EdgeInsets.symmetric(horizontal:15,vertical: 3),
                            duration: const Duration(milliseconds: 250),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: Colors.black12),
                              color: categoeryIndexSelected == index ? color : Colors.white
                            ),
                            child: Center(
                              child: Text(categories[index],textAlign: TextAlign.center,
                              style: GoogleFonts.almarai(fontSize: 12,color: categoeryIndexSelected == index ? Colors.white : Colors.black,fontWeight: categoeryIndexSelected == index ? FontWeight.bold : FontWeight.normal),),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: GridView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(horizontal: 1,vertical: 8),
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 1,
                        mainAxisExtent: 260,
                        crossAxisSpacing: 20
                    ),
                    children: products.map((Product element){
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              top: 0,
                              bottom: 40,
                              left: 10,
                              right: 10,
                              child: Container(
                                height: 110,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.red,
                                    image: DecorationImage(
                                        image: AssetImage(element.img),
                                        fit: BoxFit.cover
                                    )
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: SizedBox(
                                height: 60,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: 20.0,
                                      sigmaY: 20.0,
                                    ),
                                    child: AnimatedContainer(
                                      duration: const Duration(milliseconds: 250),
                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.95).withOpacity(0.2),
                                          borderRadius: BorderRadius.circular(20),
                                          border: Border.all(color: Colors.grey.withOpacity(0.2))
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(element.name,textAlign: TextAlign.right,
                                                style: GoogleFonts.almarai(fontSize: 15,color: Colors.black),),
                                              const SizedBox(height: 5,),
                                              Text(element.price,textAlign: TextAlign.right,
                                                style: GoogleFonts.almarai(fontSize: 15,color: Colors.green,fontWeight: FontWeight.bold),),
                                            ],
                                          ),
                                          const Icon(Icons.favorite,color: Colors.red,),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                height: 35,
                                width: 35,
                                margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.white,
                                  border: Border.all(color: Colors.black12)
                                ),
                                child: const Center(
                                  child: Icon(Icons.add_shopping_cart,color: Colors.green,size: 17,),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20,left: 15,right: 15),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                border: Border.all(color: Colors.black26.withOpacity(0.1))
              ),
              child: Row(
                children: [
                  widgetOfNavBar(Icons.settings,0),
                  widgetOfNavBar(Icons.shopping_cart,1),
                  widgetOfNavBar(Icons.home,2),
                  widgetOfNavBar(Icons.person_outline_sharp,3),
                  widgetOfNavBar(Icons.favorite_border,4),

                ],
              ),
            ),
          ),
        ],
      ),
    ),
    );
  }

  Widget widgetOfNavBar(IconData icon,int index){
    return Expanded(
      child: GestureDetector(
        onTap: (){
          setState(() {
            pageIndex = index;
          });
        },
        child: SizedBox(
          child: Column(
            children: [
              const SizedBox(height: 15,),
              Icon(icon,size: 25,color: index == pageIndex ? color : Colors.black54,),
              const SizedBox(height: 5,),
              AnimatedContainer(
                duration: const Duration(milliseconds: 250),
                height: 3,
                width: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: index == pageIndex ? color : Colors.transparent
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


}

class Product{
  String name;
  String price;
  String img;
  String description;
  Product({required this.name,required this.img,required this.description,required this.price});
}

